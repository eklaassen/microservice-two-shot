from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoes


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["id", "manufacturer", "model_name", "color", "image"]

    def get_extra_data(self, o):
        return {"wardrobe_bin": o.wardrobe_bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image",
        "wardrobe_bin",
    ]
    encoders = {
        "wardrobe_bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    # POST
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["wardrobe_bin"]
            wardrobe_bin = BinVO.objects.get(import_href=bin_href)
            print(wardrobe_bin.__dict__)
            content["wardrobe_bin"] = wardrobe_bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid bin id"}, status=400)
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
    # DELETE
    else:
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
