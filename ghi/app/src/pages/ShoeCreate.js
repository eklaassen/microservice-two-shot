import React from "react";

// function ShoeCreate() {
//   return (
//     <div className="px-4 py-5 my-5 text-center">
//       <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
//       <div className="col-lg-6 mx-auto">
//         <p className="lead mb-4">New Shoe Page</p>
//       </div>
//     </div>
//   );
// }

class ShoeCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
      model_name: "",
      color: "",
      image: "",
      wardrobe_bin: "",
      wardrobe_bins: [],
    };
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleModelNameChange = this.handleModelNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleWardrobeBinChange = this.handleWardrobeBinChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.wardrobe_bins;

    const shoesUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      const cleared = {
        manufacturer: "",
        model_name: "",
        color: "",
        image: "",
        wardrobe_bin: "",
      };
      this.setState(cleared);
    }
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  handleModelNameChange(event) {
    const value = event.target.value;
    this.setState({ model_name: value });
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleImageChange(event) {
    const value = event.target.value;
    this.setState({ image: value });
  }

  handleWardrobeBinChange(event) {
    const value = event.target.value;
    this.setState({ wardrobe_bin: value });
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      this.setState({ wardrobe_bins: data.bins });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleManufacturerChange}
                  value={this.state.manufacturer}
                  placeholder="Brand"
                  required
                  type="text"
                  name="manufacturer"
                  id="manufacturer"
                  className="form-control"
                />
                <label htmlFor="manufacturer">Brand</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleModelNameChange}
                  value={this.state.model_name}
                  placeholder="Model Name"
                  required
                  type="text"
                  name="model_name"
                  id="model_name"
                  className="form-control"
                />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleColorChange}
                  value={this.state.color}
                  placeholder="Color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleImageChange}
                  value={this.state.image}
                  placeholder="Image URL"
                  required
                  type="url"
                  name="image"
                  id="image"
                  className="form-control"
                />
                <label htmlFor="image">Image URL</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleWardrobeBinChange}
                  value={this.state.wardrobe_bin}
                  required
                  name="wardrobe_bin"
                  id="wardrobe_bin"
                  className="form-select"
                >
                  <option value="">Choose a wardrobe bin</option>
                  {this.state.wardrobe_bins.map((wardrobe_bin) => {
                    return (
                      <option key={wardrobe_bin.href} value={wardrobe_bin.href}>
                        {wardrobe_bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add Shoe</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeCreate;
