import React, { useEffect, useState } from "react";

function ShoeCard(props) {
  return (
    <div className="card col-3 my-2 mx-2">
      {props.shoe.image && (
        <img src={props.shoe.image} className="card-img-top" alt="Shoe image" />
      )}
      <div className="card-body">
        <h5 className="card-title">{props.shoe.model_name}</h5>
        <p className="card-text">
          <ul style={{ listStyle: "none" }}>
            <li>Brand: {props.shoe.manufacturer}</li>
            <li>Color: {props.shoe.color}</li>
            <li>Wardrobe Bin: {props.shoe.wardrobe_bin}</li>
          </ul>
        </p>
        <button
          onClick={() => {
            console.log(props.shoe.id);
            props.onDelete(props.shoe.id);
          }}
          className="btn btn-danger"
        >
          Delete
        </button>
      </div>
    </div>
  );
}

class ShoeList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shoes: [],
    };

    this.deleteShoe = this.deleteShoe.bind(this);
  }

  async deleteShoe(id) {
    const url = `http://localhost:8080/api/shoes/${id}`;

    try {
      const response = await fetch(url, {
        method: "DELETE",
      });

      if (response.ok) {
        this.setState((prevState) => ({
          shoes: prevState.shoes.filter((shoe) => shoe.id !== id),
        }));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/shoes/";

    try {
      const response = await fetch(url);
      const data = await response.json();
      console.log(data);
      if (response.ok) {
        this.setState({ shoes: data.shoes });
      }
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-12 mx-auto">
          <p className="lead mb-4">Shoes List</p>

          <div className="row justify-content-center">
            {this.state.shoes.map((shoe, index) => {
              return (
                <ShoeCard shoe={shoe} test={index} onDelete={this.deleteShoe} />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeList;
