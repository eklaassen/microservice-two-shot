import React from "react";

function HatCard(props) {
    return (
        <div className="card col-3 my-2 mx-2">
            {props.hat.image && (
                <img src={props.hat.image} className="card-img-top" alt="Hat image" />
            )}
            <div className="card-body">
                <h5 className="card-title">{props.hat.style_name}</h5>
                <h6 className="card-subtitle text-muted">Color: {props.hat.color}</h6>
                <h6 className="card-subtitle text-muted">Fabric: {props.hat.fabric}</h6>
                <h6 className="card-subtitle text-muted">Location: {props.hat.location}</h6>
            </div>
            <div className="card-footer">
                <button
                    onClick={() => {
                        props.onDelete(props.hat.id);
                    }}
                    className="btn btn-danger"
                >
                    Delete
                </button>
            </div>
        </div>
    );
}
  
class HatsList extends React.Component {
    constructor(props) {
        super(props);
  
        this.state = {
            hats: [],
        };
  
        this.deleteHat = this.deleteHat.bind(this);
    }
  
    async deleteHat(id) {
        const url = `http://localhost:8090/api/hats/${id}`;

        try {
            const response = await fetch(url, {
                method: "DELETE",
            });

            if (response.ok) {
                this.setState((prevState) => ({
                    hats: prevState.hats.filter((hat) => hat.id !== id),
                }));
            }
        } catch (e) {
            console.log(e);
        }
    }
  
    async componentDidMount() {
        const url = "http://localhost:8090/api/hats/";

        try {
            const response = await fetch(url);
            const data = await response.json();

            if (response.ok) {
                this.setState({ hats: data.hats });
            }
        } catch (e) {
            console.log(e);
        }
    }
  
    render() {
        return (
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
                <div className="col-lg-12 mx-auto">
                    <p className="lead mb-4">Hats!</p>

                    <div className="row justify-content-center">
                        {this.state.hats.map((hat, index) => {
                            return (
                                <HatCard 
                                hat={hat} 
                                key={hat.id} 
                                test={index} 
                                onDelete={this.deleteHat} 
                                />
                            );
                        })}
                    </div>    
                </div>
            </div>
        );
    }
}
  
  export default HatsList;


// class HatsList extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             hats: [],
//         }
//     }

//     async componentDidMount() {
//         const url = 'http://localhost:8090/api/hats/';
//         const response = await fetch(url);

//         if (response.ok) {
//             const data = await response.json();
//             this.setState({ hats: data.hats })
//         }
//     }

//     render() {
//         return (
//             <>
//                 <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
//                     <Link 
//                       to="/hats/new" 
//                       className="btn btn-primary btn-lg px-4 gap-3"
//                     >
//                         Add a hat!
//                     </Link>
//                 </div>
//                 {this.state.hats.map((hat) => (
//                     <Link 
//                       style={{ display: "block", margin: "1rem 0" }} 
//                       to={`/hats/${hat.id}`} 
//                       key={hat.id}
//                     >
//                         {hat.style_name}
//                     </Link>
//                 ))}
//             </>
//         )
//     }
// }

// export default HatsList;