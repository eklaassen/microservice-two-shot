import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            style_name: [],
            color: '',
            fabric: '',
            image: '',
            location: '',
        };
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleLocationSelection = this.handleLocationSelection.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({ styleName: value });
    }
    
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }
    
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    
    handleImageChange(event) {
        const value = event.target.value;
        this.setState({ image: value })
    }
    
    handleLocationSelection(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.style_name = data.styleName;
        delete data.styleName;
        delete data.locations;

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            const cleared = {
                styleName: '',
                color: '',
                fabric: '',
                image: '',
                location: '',
            };
            this.setState(cleared)
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input 
                                  className="form-control" 
                                  onChange={this.handleStyleNameChange} 
                                  value={this.state.styleName} 
                                  placeholder="Style name" 
                                  required 
                                  type="text" 
                                  name="style_name" 
                                  id="style_name"
                                />
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                  className="form-control" 
                                  onChange={this.handleColorChange} 
                                  value={this.state.color} 
                                  placeholder="Color" 
                                  required 
                                  type="text" 
                                  name="color" 
                                  id="color"
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                  className="form-control" 
                                  onChange={this.handleFabricChange} 
                                  value={this.state.fabric} 
                                  placeholder="Fabric" 
                                  required 
                                  type="text" 
                                  name="fabric" 
                                  id="fabric"
                                />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                  className="form-control" 
                                  onChange={this.handleImageChange} 
                                  value={this.state.image} 
                                  placeholder="Image URL" 
                                  required 
                                  type="textarea" 
                                  name="image" 
                                  id="image"
                                />
                                <label htmlFor="image">Image URL</label>
                            </div>
                            <div className="mb-3">
                                <select 
                                  onChange={this.handleLocationSelection} 
                                  value={this.state.location} 
                                  required 
                                  id="location" 
                                  name="location" 
                                  className="form-select"
                                >
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default HatForm;