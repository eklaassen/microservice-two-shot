import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoeList from "./pages/ShoeList";
import ShoeCreate from "./pages/ShoeCreate";
import HatsList from './pages/HatsList';
import HatForm from './pages/HatForm';

function App() { 
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/* <Route index element={<MainPage />} /> */}
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/shoes/new" element={<ShoeCreate />} />
          <Route path="hats">
            <Route path="" element={ <HatsList /> } />
            <Route path='new' element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
