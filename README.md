# Wardrobify

Team:

- Person 1 - Which microservice?
  Elliott - Hat
- Person 2 - Which microservice?
  Cindy - Shoe

## Design

There will be bins and locations within the wardrobe that will act as containers for the shoes and hats microservices, respectively. The hats and shoes will be connected to these locations and bins through foreign keys on their respective models.

## Shoes microservice

A lot of referencing previous projects (Conference-Go and Fearless-front-end).
Models properties grabbed from the Learn, made a BinsVO to grab bins from the wardrobe, work though API views using encoders to have all of the properties in the model. Polling was rough. :( React - decided to make the shoes list into cards with details/delete on them, they turned out super nice!

## Hats microservice

I am going to create a Hat model that has fields for its style name, the fabric used, its color, a URL of a picture of the hat, and the location in the wardrobe where it exists.

I am also going to create a LocationVO object since I only need to pull relevant information from that separate microservice to implement with my Hats model.

Then, I will grab the data that I need from RESTful APIs I will build for the hats that I can use in my microservices as well as the front end components.
